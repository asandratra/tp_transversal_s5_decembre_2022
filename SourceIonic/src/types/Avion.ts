export default interface AvionData {
    id?: any | null,
    idmodele: any,
    estactif: any,
    expiration_assurance: Date,
    immatriculation: string
  }