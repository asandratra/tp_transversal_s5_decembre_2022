export default interface AvionDetailsData {
    id?: any | null,
    modele: string,
    matricule: string,
    daty: Date,
    debut: any,
    fin: any,
    estactif: any,
    expiration_assurance: Date
}