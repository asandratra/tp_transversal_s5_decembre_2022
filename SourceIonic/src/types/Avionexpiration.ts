export default interface AvionexpireData {
    id?: any | null,
    diff: any,
    modele: string,
    matricule: string,
    estactif: any
  }