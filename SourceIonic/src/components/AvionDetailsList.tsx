import React, { Component } from 'react';
import AvionDetailsData from '../types/AvionDetails';



type Props = {};

type State = {
    Avions: Array<AvionDetailsData>
};
export default class AvionDetailsList extends Component<Props, State> {

    constructor(props: Props){
        super(props);

   
        this.retrieveAvion();

        this.state = {
          Avions: []
          };
    }
   

    retrieveAvion() {
        const id = localStorage.getItem("id");
        fetch("http://localhost:1510/avionDetails/"+id)
          .then(response => response.json())
          .then((Avions: any) => {
            this.setState({
              Avions: Avions.data
            })
        }) }

      render() {
        const Avions = this.state.Avions;
     
        return (
          <div className="list row">
            
            <div className="col-md-6">
              <h4>Avion Details List</h4>
            
              <ul className="list-group">
                {
                  
                  Avions.map((Avion: AvionDetailsData) => (
                <li key={Avion.id}>
                     <h4> {Avion.id} matricule: {Avion.matricule} assurance expiration: {Avion.expiration_assurance} Daty: {Avion.daty} Debut: {Avion.debut}km Fin: {Avion.fin}km est actif: {Avion.estactif}</h4> 
                      </li>

                    
                  ))}
              </ul>
    
              
             
            </div>
            
            <br />

            <br />
            <a href="/">Voir Avion list</a>
          </div>
        );
      }

 
}

