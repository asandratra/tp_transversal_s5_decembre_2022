import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AvionexpireData from '../types/Avionexpiration';



type Props = {};

type State = {
    Avions: Array<AvionexpireData>
};
export default class AvionList extends Component<Props, State> {

    constructor(props: Props){
        super(props);

   
        this.retrieveAvion();

        this.state = {
          Avions: []
          };
    }
   

    retrieveAvion() {
        const diff = localStorage.getItem("diff");
        fetch("http://localhost:1510/avionexpiration/"+diff)
          .then(response => response.json())
          .then((Avions: any) => {
            this.setState({
              Avions: Avions.data
            })
        }) }

      render() {
        const Avions = this.state.Avions;
     
        return (
          <div className="list row">
            
            <div className="col-md-6">
              <h4>Avion List</h4>
    
              <ul className="list-group">
                {
                  
                  Avions.map((Avion: AvionexpireData) => (
                    <Link to={"/"}><li key={Avion.id}>
                     <h4> {Avion.id} matricule: {Avion.matricule} Expiration: dans {Avion.diff}Mois est actif: {Avion.estactif}</h4> 
                      </li>
                      </Link> 
                    
                  ))}
              </ul>
    
              
             
            </div>
            <br />

            <br />
            <a href="/">Voir Avion list</a>
          </div>
        );
      }

 
}

