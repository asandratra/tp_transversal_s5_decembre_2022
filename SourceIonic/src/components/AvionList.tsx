
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AvionData from '../types/Avion';




type Props = {};

type State = {
    Avions: Array<AvionData>
};

export default class AvionList extends Component<Props, State> {

    constructor(props: Props){
        super(props);

   
        this.retrieveAvion();

        this.state = {
          Avions: []
          };
    }
   

    retrieveAvion() {
        fetch("http://localhost:1510/avion")
          .then(response => response.json())
          .then((Avions: any) => {
            this.setState({
              Avions: Avions.data
            })
        }) }

      render() {
        const Avions = this.state.Avions;
     
       

        function getById(id: any): void {
          localStorage.setItem("id",id);
        }

        function getByExpire3(arg0: any): void {
          localStorage.setItem("diff",arg0);
        }
        function getByExpire1(arg0: any): void {
          localStorage.setItem("diff",arg0);
        }
        return (
          <div className="list row">
            
            <div className="col-md-6">
              <h4>Avion List</h4>
    
              <ul className="list-group">
                {
                  
                  Avions.map((Avion: AvionData) => (
                    
                    <Link onClick={(e:any)=> getById(Avion.id)} to={"/Login"}><li key={Avion.id}>
                      
                     <h4> {Avion.id} matricule: {Avion.immatriculation} assurance expiration: {Avion.expiration_assurance} est actif: {Avion.estactif}</h4> 
                      </li>
                      </Link> 
                    
                  ))}
              </ul>
    
              
             
            </div>
            <div>
                    <h3>Expiration</h3>
                    <a onClick={(e:any)=> getByExpire1(1)} href="/AvionExpiration">Expire dans 1Mois</a>
                    <br />
                    <br />
                    <a onClick={(e:any)=> getByExpire3(3)} href="/AvionExpiration">Expire dans 3Mois</a>
            </div>
          </div>
          
        );

      }

 
}

