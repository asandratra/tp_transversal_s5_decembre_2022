package tpws.wsproject.Controller;


import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tpws.wsproject.Entities.Utilisateur;
import tpws.wsproject.Services.UserService;

@RestController
@RequestMapping("/Utilisateur")
public class UserController {

	
	@Autowired
    private UserService service;

    

    @PostMapping
    public String saveUser(@RequestBody Utilisateur user) {
        service.save(user);
        return "Insertion succes";
    }

    @PutMapping("{Id}")
    public ResponseEntity<Utilisateur> UpdateUser(@PathVariable int Id,@RequestBody Utilisateur usermodified) {
    	Utilisateur user = service.get(Id);
    	user.setUsername(usermodified.getUsername());
    	user.setNom(usermodified.getNom());
    	user.setPrenom(usermodified.getPrenom());
    	user.setEmail(usermodified.getEmail());
    	user.setPwd(usermodified.getPwd());
		service.get(Id);
		return ResponseEntity.ok(user);
	}

    @DeleteMapping("{Id}")
    public ResponseEntity<HttpStatus> deleteUser(@PathVariable int Id) {
        service.delete(Id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    
    @GetMapping
    public Utilisateur findUser(@RequestBody Utilisateur user) throws NoSuchAlgorithmException
    {
        Utilisateur u = service.findUser(user.getUsername(),user.getPwd());
        if(u!=null) {
        	u.generateToken();
        }
        return u;
    }
}
