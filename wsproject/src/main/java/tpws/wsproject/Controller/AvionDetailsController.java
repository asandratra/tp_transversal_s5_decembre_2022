package tpws.wsproject.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tpws.wsproject.Entities.AvionDetails;
import tpws.wsproject.Services.AvionDetailsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequestMapping("/avionDetails")
public class AvionDetailsController {

    @Autowired
    private AvionDetailsService service;

    @GetMapping
    public Map<String, Object> ListeAvionDetails() {
        Map<String, Object> data = new HashMap<String, Object>();
        List<AvionDetails> listAviondetails = service.listAll();
        data.put("data", listAviondetails);
        return data;
    }

    @PostMapping
    public AvionDetails saveAvionDetails(@RequestBody AvionDetails aviondetails) {
        service.save(aviondetails);
        return aviondetails;
    }

    @PutMapping("{Id}")
    public ResponseEntity<AvionDetails> UpdateAvionDetails(@PathVariable int Id,
            @RequestBody AvionDetails avion) {
        AvionDetails avi = service.get(Id);
        avi.setModel(avion.getModel());
        avi.setImmatriculation(avion.getImmatriculation());
        avi.setEstactif(avion.isEstactif());
        avi.setDaty(avion.getDaty());
        avi.setDebut(avion.getDebut());
        avi.setIdKilometrage(avion.getIdKilometrage());
        avi.setFin(avion.getFin());
        avi.setExpiration_assurance(avion.getExpiration_assurance());
        service.save(avi);
        return ResponseEntity.ok(avi);
    }

    @DeleteMapping("{Id}")
    public ResponseEntity<AvionDetails> deleteAvionDetails(@PathVariable int Id) {
        service.delete(Id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    
    @GetMapping("{id}")
    public Map<String,Object> findAvion(@PathVariable int id) {
    	Map<String, Object> data = new HashMap<String, Object>();
    	List<AvionDetails> list = service.list(id);
    	data.put("data",list);
    	
    	return data;
    	
    }
}
