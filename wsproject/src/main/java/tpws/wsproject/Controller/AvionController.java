package tpws.wsproject.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tpws.wsproject.Entities.Avion;
import tpws.wsproject.Services.AvionService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequestMapping("/avion")
public class AvionController {
	
    @Autowired
    private AvionService service;

    @GetMapping
    public Map<String,Object> ListeAvion()
    {
    	Map<String, Object> data = new HashMap<String, Object>();
        List<Avion> listavion = service.listAll();
        data.put("data", listavion);
        return data;
    }

    @PostMapping
    public Avion saveAvion(@RequestBody Avion avion) {
        service.save(avion);
        return avion;
    }

    @PutMapping("{Id}")
    public ResponseEntity<Avion> UpdateAvion(@PathVariable int Id,@RequestBody Avion av) {
    	Avion avion = service.get(Id);
    	avion.setIdModel(av.getIdModel());
    	avion.setImmatriculation(av.getImmatriculation());
    	avion.setEstactif(av.isEstactif());
    	avion.setExpiration_assurance(av.getExpiration_assurance());
		service.save(avion);
		return ResponseEntity.ok(avion);
	}

    @DeleteMapping("{Id}")
    public ResponseEntity<Avion> deleteAvion(@PathVariable int Id) {
        service.delete(Id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    
    @GetMapping("{Id}")
    public Avion findAvion(@PathVariable int Id) {
    	Avion avi = service.get(Id);
    	
    	return avi;
    }
}
