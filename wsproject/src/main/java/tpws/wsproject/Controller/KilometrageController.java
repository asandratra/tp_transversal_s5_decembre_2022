package tpws.wsproject.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tpws.wsproject.Entities.Kilometrage;
import tpws.wsproject.Services.KilometrageService;


@RestController
@RequestMapping("/Kilometrage")
public class KilometrageController {

	
	@Autowired
    private KilometrageService service;

    @GetMapping
    public List<Kilometrage> ListKilometrage()
    {
        List<Kilometrage> listKilometrage = service.listAll();
        return listKilometrage;
    }

    @PostMapping
    public String saveKilometrage(@RequestBody Kilometrage kilometrage) {
        service.save(kilometrage);
        return "Insertion succes";
    }

    @PutMapping("{Id}")
    public ResponseEntity<Kilometrage> UpdateKilometrage(@PathVariable(name = "id") int id,@RequestBody Kilometrage kilometragemodified) {
    	Kilometrage kilometrage = service.get(id);
    	kilometrage.setIdVehicule(kilometragemodified.getIdVehicule());
    	kilometrage.setDaty(kilometragemodified.getDaty());
    	kilometrage.setDebut(kilometragemodified.getDebut());
    	kilometrage.setFin(kilometragemodified.getFin());
		service.save(kilometrage);
		return ResponseEntity.ok(kilometrage);
	}

    @DeleteMapping("{Id}")
    public ResponseEntity<HttpStatus> deleteKilometrage(@PathVariable(name = "id") int id) {
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
