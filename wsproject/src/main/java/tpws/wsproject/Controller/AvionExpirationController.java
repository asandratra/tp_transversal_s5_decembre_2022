package tpws.wsproject.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tpws.wsproject.Entities.Avionexpiration;
import tpws.wsproject.Services.AvionexpireService;

@CrossOrigin
@RestController
@RequestMapping("avionexpiration")
public class AvionExpirationController {
	
	@Autowired
	private AvionexpireService service;

	@GetMapping
    public Map<String,Object> ListeAvionexpiration()
    {
    	Map<String, Object> data = new HashMap<String, Object>();
        List<Avionexpiration> listavion = service.getAll();
        data.put("data", listavion);
        return data;
    }
	
	@GetMapping("{Diff}")
	public Map<String,Object> AvionexpirationBydiff(@PathVariable int Diff)
    {
    	Map<String, Object> data = new HashMap<String, Object>();
        List<Avionexpiration> listavion = service.findByDiff(Diff);
        data.put("data", listavion);
        return data;
    }
	
}
