package tpws.wsproject.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="avionexpiration")
public class Avionexpiration {

	@Id
	private int Id;
	
	@Column(name="diff")
	private double diff;
	
	@Column(name="modele")
	private String modele;
	
	@Column(name="matricule")
	private String matricule;
	
	
	@Column(name="estactif")
	private boolean estactif;


	public int getId() {
		return Id;
	}


	public void setId(int id) {
		Id = id;
	}


	public double getDiff() {
		return diff;
	}


	public void setDiff(double diff) {
		this.diff = diff;
	}


	public String getModele() {
		return modele;
	}


	public void setModele(String modele) {
		this.modele = modele;
	}


	public String getMatricule() {
		return matricule;
	}


	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}


	public boolean isEstactif() {
		return estactif;
	}


	public void setEstactif(boolean estactif) {
		this.estactif = estactif;
	}
}
