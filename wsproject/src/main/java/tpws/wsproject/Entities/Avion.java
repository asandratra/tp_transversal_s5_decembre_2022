package tpws.wsproject.Entities;

import javax.persistence.Id;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "avion")
public class Avion {
	
	@Id
    private Integer Id;
	
	@Column(name = "idmodele")
    private int idModele;
	
	@Column(name = "matricule")
    private String matricule;
	
	@Column(name="expiration_assurance")
	private Date expiration_assurance;

	@Column(name = "estactif")
	private boolean estactif;

	
	public int getId() {
        return Id;
    }

    public void setIdModel(int idModel) {
		this.idModele = idModel;
	}

	public void setId(int id) {
        Id = id;
    }

	public int getIdModel() {
		return idModele;
	}

	
	public Date getExpiration_assurance() {
		return expiration_assurance;
	}

	public void setExpiration_assurance(Date expiration_assurance) {
		this.expiration_assurance = expiration_assurance;
	}

	public boolean isEstactif() {
		return estactif;
	}

	public void setEstactif(boolean estactif) {
		this.estactif = estactif;
	}

	public String getImmatriculation() {
		return matricule;
	}

	public void setImmatriculation(String immatriculations) {
		matricule = immatriculations;
	}
}
