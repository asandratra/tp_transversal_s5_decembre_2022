package tpws.wsproject.Entities;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="utilisateur")
public class Utilisateur{

	
	@Id
	private Integer Id;
	
	@Column(name="username")
	private String username;
	
	@Column(name="nom")
	private String nom;

	@Column(name="prenom")
	private String prenom;
	
	@Column(name="email")
	private String email;
	
	@Column(name="pwd")
	private String pwd;
	
	
	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	
	public String generateToken() throws NoSuchAlgorithmException{
        
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			
			byte[] mdigest = md.digest((this.getEmail()+this.getPwd()+LocalDate.now()).getBytes());
			
			BigInteger no = new BigInteger(1,mdigest);
			
			StringBuilder hashtext = new StringBuilder(no.toString(16));
			
			while(hashtext.length()<32) {
				hashtext.insert(0, "0");
			}
			
			return hashtext.toString();
			
		} catch (RuntimeException e) {
			
			throw new RuntimeException(e);
		}
		
    }
	
}
