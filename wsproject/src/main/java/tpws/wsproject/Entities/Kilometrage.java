package tpws.wsproject.Entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "kilometrage")
public class Kilometrage {
	
	@Id
	private int Id;
	
	@Column(name ="idvehicule")
	private int idVehicule;
	
	@Column(name="daty")
	private Date daty;
	
	@Column(name="debut")
	private double debut;
	
	@Column(name="fin")
	private double fin;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public int getIdVehicule() {
		return idVehicule;
	}

	public void setIdVehicule(int idVehicule) {
		this.idVehicule = idVehicule;
	}

	public Date getDaty() {
		return daty;
	}

	public void setDaty(Date daty) {
		this.daty = daty;
	}

	public double getDebut() {
		return debut;
	}

	public void setDebut(double debut) {
		this.debut = debut;
	}

	public double getFin() {
		return fin;
	}

	public void setFin(double fin) {
		this.fin = fin;
	}
	
	
	
}
