package tpws.wsproject.Entities;

import javax.persistence.Id;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "aviondetails")
public class AvionDetails {

    @Column(name="id")
    private Integer Id;


    @Column(name = "modele")
    private String model;

    @Column(name = "matricule")
    private String matricule;

    @Column(name = "estactif")
    private boolean estactif;

    @Id
    @Column(name = "kilometrageid")
    private int idKilometrage;

    @Column(name = "daty")
    private Date daty;

    @Column(name = "debut")
    private double debut;
    
	@Column(name = "expiration_assurance")
    private Date expiration_assurance;

    @Column(name = "fin")
    private double fin;
    
    public Date getExpiration_assurance() {
		return expiration_assurance;
	}

	public void setExpiration_assurance(Date expiration_assurance) {
		this.expiration_assurance = expiration_assurance;
	}



    public int getId() {
        return Id;
    }

    public void setModel(String Model) {
        this.model = Model;
    }

    public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public int getIdKilometrage() {
		return idKilometrage;
	}

	public void setIdKilometrage(int idKilometrage) {
		this.idKilometrage = idKilometrage;
	}

	public Date getDaty() {
		return daty;
	}

	public void setDaty(Date daty) {
		this.daty = daty;
	}

	public double getDebut() {
		return debut;
	}

	public void setDebut(double debut) {
		this.debut = debut;
	}

	public double getFin() {
		return fin;
	}

	public void setFin(double fin) {
		this.fin = fin;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public void setId(int id) {
        Id = id;
    }

    public String getModel() {
        return model;
    }

    public boolean isEstactif() {
        return estactif;
    }

    public void setEstactif(boolean estactif) {
        this.estactif = estactif;
    }

    public String getImmatriculation() {
        return matricule;
    }

    public void setImmatriculation(String immatriculations) {
        matricule = immatriculations;
    }
}
