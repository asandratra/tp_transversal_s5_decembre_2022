package tpws.wsproject.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tpws.wsproject.Entities.Avion;
import tpws.wsproject.Repositories.AvionRepository;
import java.util.List;

@Service
public class AvionService {
    @Autowired
    private AvionRepository repo;

    public List<Avion> listAll() {
        return repo.findAll();
    }

    public void save(Avion avion) {
    	repo.save(avion);
    }

    public Avion get(int id) {
        return repo.findById(id).get();
    }

    public void delete(int id) {
    	repo.deleteById(id);
    }
}
