package tpws.wsproject.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tpws.wsproject.Entities.Kilometrage;
import tpws.wsproject.Repositories.KilometrageRepository;

@Service
public class KilometrageService {

	@Autowired
	private KilometrageRepository kilometragerepo;
	
	public List<Kilometrage> listAll() {
        return kilometragerepo.findAll();
    }

    public void save(Kilometrage kilometrage) {
    	kilometragerepo.save(kilometrage);
    }

    public Kilometrage get(int id) {
        return kilometragerepo.findById(id).get();
    }

    public void delete(int id) {
    	kilometragerepo.deleteById(id);
    }
}
