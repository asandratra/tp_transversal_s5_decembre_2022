package tpws.wsproject.Services;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tpws.wsproject.Entities.Avionexpiration;
import tpws.wsproject.Repositories.AvionexpireRepository;

@Service
public class AvionexpireService {

	
	@Autowired
	private AvionexpireRepository repo;
	
	
	public List<Avionexpiration> getAll(){	
		
		return repo.findAll();
	
	}
	
	
	public List<Avionexpiration> findByDiff(int diff){
		
		return repo.findAllByAvionDiff(diff);
		
		
	}
}
