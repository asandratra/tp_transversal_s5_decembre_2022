package tpws.wsproject.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tpws.wsproject.Entities.AvionDetails;
import tpws.wsproject.Repositories.AvionDetailsRepository;

import java.util.List;

@Service
public class AvionDetailsService {
    @Autowired
    private AvionDetailsRepository vehiculedetailsRepository;

    public List<AvionDetails> listAll() {
        return vehiculedetailsRepository.findAll();
    }

    public void save(AvionDetails vehiculedetails) {
        vehiculedetailsRepository.save(vehiculedetails);
    }
    
    public List<AvionDetails> list(int id){
    	return vehiculedetailsRepository.findAllByAvionId(id);
    }

    public AvionDetails get(int id) {
        return vehiculedetailsRepository.findById(id).get();
    }

    public void delete(int id) {
        vehiculedetailsRepository.deleteById(id);
    }
}
