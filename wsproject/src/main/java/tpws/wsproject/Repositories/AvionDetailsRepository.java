package tpws.wsproject.Repositories;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tpws.wsproject.Entities.AvionDetails;

public interface AvionDetailsRepository extends JpaRepository<AvionDetails, Integer> {

	@Query(value = "SELECT * FROM AvionDetails ad WHERE ad.id = :id", nativeQuery = true)
	public List<AvionDetails> findAllByAvionId(@Param("id") int id);

}
