package tpws.wsproject.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import tpws.wsproject.Entities.Kilometrage;

public interface KilometrageRepository extends JpaRepository<Kilometrage, Integer>{

}
