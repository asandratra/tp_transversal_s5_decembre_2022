package tpws.wsproject.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tpws.wsproject.Entities.Avionexpiration;

public interface AvionexpireRepository extends JpaRepository<Avionexpiration, Integer>{

	@Query(value = "SELECT * FROM avionexpiration ae WHERE ae.diff = :diff", nativeQuery = true)
	List<Avionexpiration> findAllByAvionDiff(@Param("diff")int diff);
}
