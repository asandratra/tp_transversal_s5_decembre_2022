package tpws.wsproject.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tpws.wsproject.Entities.Utilisateur;

public interface UserRepository extends JpaRepository<Utilisateur, Integer>{

	@Query("Select u from Utilisateur u where (:username is null or u.username=:username) and (:pwd is null or u.pwd=:pwd)")
	public Utilisateur findByUsernameAndPwd(@Param("username")String username,@Param("pwd")String pwd);
}
