CREATE SEQUENCE modele_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE utilisateur_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE avion_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE kilometrage_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE token_id_seq START WITH 1 INCREMENT BY 1;

CREATE TABLE Modele (
	id    integer DEFAULT nextval('modele_id_seq'::regclass) NOT NULL  ,
	nom   varchar(50) UNIQUE NOT NULL ,
	CONSTRAINT pk_modele PRIMARY KEY(id)
);

CREATE TABLE Avion (
	id            integer DEFAULT nextval('avion_id_seq'::regclass) NOT NULL  ,
	idModele      integer NOT NULL ,
	matricule     varchar(50) UNIQUE NOT NULL,
	estactif      boolean NOT NULL DEFAULT TRUE,
	expiration_assurance DATE NOT NULL,
	CONSTRAINT pk_avion PRIMARY KEY(id),
	CONSTRAINT fk_avion FOREIGN KEY(idModele) REFERENCES Modele(id)
);

CREATE TABLE Kilometrage(
	id            integer DEFAULT nextval('kilometrage_id_seq'::regclass) NOT NULL  ,
	idAvion    integer NOT NULL,
	daty          date    NOT NULL CHECK(daty <= CURRENT_DATE),
	debut         real  NOT NULL,
	fin           real  NOT NULL,
	CONSTRAINT pk_kilometrage PRIMARY KEY(id),
	CONSTRAINT fk_kilometrage FOREIGN KEY(idAvion) REFERENCES Avion(id)
);	

CREATE  TABLE utilisateur ( 
	id            integer DEFAULT nextval('utilisateur_id_seq'::regclass) NOT NULL  ,
	username      varchar(50)  UNIQUE NOT NULL  ,
	nom           varchar(50)  NOT NULL  ,
	prenom        varchar(50)  NOT NULL  ,
	email         varchar(50)  UNIQUE NOT NULL CHECK ( email ~ '^[a-zA-Z0-9.!#$%&''*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$' ),
	pwd           varchar(55)  NOT NULL  ,
	CONSTRAINT pk_utilisateur PRIMARY KEY ( id )
);
 
create or replace view AvionDetails as select Avion.id as
 id, modele.Nom as modele,Avion.matricule, Avion.estactif, Avion.expiration_assurance,kilometrage.id
 as kilometrageid,kilometrage.daty,kilometrage.debut,kilometrage.fin from Avion 
 join kilometrage on kilometrage.idAvion = Avion.id join modele on modele.id=Avion.idmodele;

 
CREATE TABLE token (
id            integer DEFAULT nextval('token_id_seq'::regclass) NOT NULL  ,
idUtilisateur    integer NOT NULL ,
date_expiration date NOT NULL,
CONSTRAINT pk_token PRIMARY KEY(id),
CONSTRAINT fk_token FOREIGN KEY(idUtilisateur) REFERENCES Utilisateur(id)
);

create or replace view tokenuser as select utilisateur.id
as iduser, token.id as tokenid,
token.date_expiration from token join utilisateur on utilisateur.id = token.idUtilisateur;


create or replace view avionexpiration as
select Avion.id,CAST((extract(epoch from (expiration_assurance - now())/(3600*24*30))) as INTEGER) 
as diff,Modele.nom as modele,matricule,Avion.estactif from Avion join 
Modele on Modele.id=Avion.idModele;